import React, { useState } from 'react';
import { useAppDispatch, useAppSelector } from '../store';
import { StarWarsStore } from '../store/StarWarsStore';
import { useService } from '../hooks/useService';
import { Layout } from '../layouts/Layout';
import { ApiRequest, ApiRequestState } from '../models/ApiRequest';
import { Person } from '../models/Person';

const personSwitch = (p: ApiRequest<Person>) => {
  switch (p.type) {
    case ApiRequestState.result:
      return <div className="message">{p.value.name}</div>;
    case ApiRequestState.error:
      return <div className="message">Error: {p.value}</div>;
    case ApiRequestState.loading:
      return <div className="message">Loading...</div>;
    default:
      return <div className="message">Not loaded</div>;
  }
};

export const HomePage: React.FunctionComponent = () => {
  const dispatch = useAppDispatch();
  const { selectors, actions } = useService(StarWarsStore);
  const person = useAppSelector(selectors.person);
  const [id, setId] = useState('1');

  const loadPerson = () => {
    dispatch(actions.loadPerson(+id));
  };

  return (
    <Layout navbar>
      <h1>Home page</h1>
      <p>
        From curious and my door. chamber sought beating Presently came word,
        the back each napping, scarce here I, faintly door. a the mortal
        suddenly you But came visiter wrought distinctly silken Thrilled rare
        name darkness and And was or I token. Some chamber books This my at it
        the now, Lenore! whispered gently midnight some before; visiter tapping
        morrow;—vainly me, lore. late each for echo quaint sure the weary.
        murmured the I Only dreaming fearing. the entreating my that and that my
        at and Doubting, whispered, the So heard nothing is, stood Deep my the
        more. Ah, truly ever borrow. I some But volume word wished this, the
        <a href="/link">link</a>
      </p>

      <button type="button" onClick={() => dispatch({ type: 'myAction' })}>
        test
      </button>

      <input
        placeholder="Person Id"
        value={id}
        onChange={(e) => setId(e.target.value)}
      />
      <button type="button" color="teal" onClick={loadPerson}>
        load
      </button>
      {personSwitch(person)}
    </Layout>
  );
};
