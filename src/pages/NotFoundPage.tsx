import React from 'react';
import { Layout } from '../layouts/Layout';

export const NotFoundPage: React.FunctionComponent = () => (
  <Layout>
    <h1>Page not found</h1>
    <p>The page you requested cannot be found!</p>
  </Layout>
);
