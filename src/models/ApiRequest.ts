export enum ApiRequestState {
  empty = 'empty',
  loading = 'loading',
  result = 'result',
  error = 'error',
}

export interface ApiEmpty {
  type: ApiRequestState.empty;
}
export interface ApiLoading {
  type: ApiRequestState.loading;
}
export interface ApiResult<T> {
  type: ApiRequestState.result;
  value: T;
}
export interface ApiError {
  type: ApiRequestState.error;
  value: string;
}

export const newApiEmpty = (): ApiEmpty => {
  return {
    type: ApiRequestState.empty,
  };
};
export const newApiLoading = (): ApiLoading => {
  return {
    type: ApiRequestState.loading,
  };
};
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const newApiResult = (value: any): ApiResult<typeof value> => {
  return {
    type: ApiRequestState.result,
    value,
  };
};
export const newApiError = (value: string): ApiError => {
  return {
    type: ApiRequestState.error,
    value,
  };
};

export type ApiRequest<T> = ApiEmpty | ApiLoading | ApiResult<T> | ApiError;
