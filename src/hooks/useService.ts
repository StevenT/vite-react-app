import React, { useContext } from 'react';
import { Injector, Provider } from '../services/injection/index';

const Context = React.createContext<Injector | undefined>(undefined);

export const InjectionProvider = Context.Provider;

export function useService<T>(provider: Provider<T>): T {
  const context = useContext(Context);
  if (!context) {
    throw new Error('WOAH! please use 1 Injector context!');
  }
  return context.resolve(provider);
}
