import { Person } from '../../models/Person';
import { registerService } from '../injection';
import { pick } from '../../utils/picker';

export class StarWarsService {
  url = 'https://swapi.dev/api';

  loadPerson = async (id: number): Promise<Person> => {
    const res = await fetch(`${this.url}/people/${id}/`);
    if (!res.ok) {
      throw new Error(`Error loading Person: ${res.status}`);
    }
    const json = await res.json();
    return pick(json, 'name') as Person;
  };
}

registerService(StarWarsService);
