import { ServiceProvider } from './Injector';

export const ServiceKey = Symbol('this class is a service');

export const registerService = (service: ServiceProvider<unknown>): void => {
  Reflect.defineMetadata(ServiceKey, true, service);
  // eslint-disable-next-line no-console
  console.debug(`Registered service [${service.name}]`);
};
