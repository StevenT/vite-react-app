import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import { App } from './App';
import { initializeStore } from './store';
import './scss/index.scss';
import { Injector } from './services/injection';
import { InjectionProvider } from './hooks/useService';

const injector = new Injector();

ReactDOM.render(
  <React.StrictMode>
    <Provider store={injector.resolve(initializeStore)}>
      <BrowserRouter>
        <InjectionProvider value={injector}>
          <App />
        </InjectionProvider>
      </BrowserRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
