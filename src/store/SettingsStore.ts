import { createSlice } from '@reduxjs/toolkit';

export interface State {
  enableDeveloperMode: boolean;
}

const initialState: State = {
  enableDeveloperMode: false,
};

export const SettingsStore = () => {
  const slice = createSlice({
    name: 'settings',
    initialState,
    reducers: {
      toggleDeveloperMode(state: State) {
        state.enableDeveloperMode = !state.enableDeveloperMode;
      },
    },
  });

  const selectors = {};
  return {
    ...slice,
    selectors,
    initialState,
  };
};
