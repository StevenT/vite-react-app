import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
  ApiRequest,
  newApiEmpty,
  newApiError,
  newApiLoading,
  newApiResult,
} from '../models/ApiRequest';
import { StarWarsService } from '../services/starwars/StarWarsService';
import { Person } from '../models/Person';

export interface State {
  person: ApiRequest<Person>;
}

const initialState: State = {
  person: newApiEmpty(),
};

export const StarWarsStore = () => {
  const service: StarWarsService = new StarWarsService();

  const loadPerson = createAsyncThunk<Person, number, { rejectValue: string }>(
    'starWars/loadPerson',
    async (id: number, { rejectWithValue }) => {
      try {
        return await service.loadPerson(id);
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error('Could not load Person', e);
        return rejectWithValue(`Could not load Person ${e.message}`);
      }
    }
  );

  const slice = createSlice({
    name: 'starWars',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
      builder.addCase(loadPerson.pending, (state) => {
        state.person = newApiLoading();
      });
      builder.addCase(loadPerson.fulfilled, (state, action) => {
        state.person = newApiResult(action.payload);
      });
      builder.addCase(loadPerson.rejected, (state, action) => {
        if (action.payload) {
          state.person = newApiError(action.payload);
        }
      });
    },
  });

  const actions = {
    ...slice.actions,
    loadPerson,
  };

  const selectors = {
    person: (state: { starWars: State }) => state.starWars.person,
  };

  return {
    ...slice,
    initialState,
    actions,
    selectors,
  };
};
