import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit';
import { useSelector } from 'react-redux';
import { SettingsStore } from './SettingsStore';
import { StarWarsStore } from './StarWarsStore';
import { Injector } from '../services/injection';
import { useService } from '../hooks/useService';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const initializeStore = (injector: Injector) => {
  return configureStore({
    reducer: {
      settings: injector.resolve(SettingsStore).reducer,
      starWars: injector.resolve(StarWarsStore).reducer,
    },
  });
};

export type Store = ReturnType<typeof initializeStore>;
export type RootState = ReturnType<Store['getState']>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

export const useAppDispatch = (): Store['dispatch'] => {
  return useService(initializeStore).dispatch;
};

export const useAppSelector = <T>(selector: (state: RootState) => T): T => {
  return useSelector<RootState, T>(selector);
};
