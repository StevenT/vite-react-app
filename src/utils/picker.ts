// eslint-disable-next-line @typescript-eslint/ban-types
export const pick = (obj: Object, ...keys: string[]) =>
  Object.fromEntries(Object.entries(obj).filter(([key]) => keys.includes(key)));
