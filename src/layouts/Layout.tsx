import React, { ReactNode } from 'react';
import { Navbar } from '../components/Navbar';

interface Props {
  navbar?: boolean;
  children: ReactNode;
}
export const Layout: React.FunctionComponent<Props> = ({
  navbar,
  children,
}) => (
  <>
    {navbar && <Navbar />}
    <div className="main">{children}</div>
  </>
);
