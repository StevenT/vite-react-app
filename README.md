# Vite React App

### Setup with

- Vite React-App
- Typescript
- SCSS
- Redux
- DevTools
- React-Router
- ESLint
- prettier formatting
- Jest
- Coverage
- CI pipeline
- SonarCloud

