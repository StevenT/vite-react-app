import React from 'react';
import { App } from '../src/App';
import { render } from './TestContext';

describe('main pages', () => {
  test('renders home page', () => {
    const { getByText } = render(<App />, { navigation: true });
    expect(getByText('Home page'));
  });

  test('renders about page', () => {
    const { getByText } = render(<App />, { navigation: true, path: '/about' });
    expect(getByText('About page'));
  });

  test('renders not found page', () => {
    const { getByText } = render(<App />, {
      navigation: true,
      path: '/no-existing-url',
    });
    expect(getByText('Page not found'));
  });
});
