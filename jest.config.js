module.exports = {
  setupFilesAfterEnv: ['<rootDir>/__tests__/jest.setup.js'],
  coverageDirectory: 'coverage',
  coveragePathIgnorePatterns: ['__tests__'],
  collectCoverageFrom: ['src/**/*.{js,jsx,ts,tsx}', '!src/**/*.d.ts'],
  testMatch: [
    '<rootDir>__tests__/**/*.{js,jsx,ts,tsx}',
    // '<rootDir>/src/**/*.{spec,test}.{js,jsx,ts,tsx}',
  ],
  testPathIgnorePatterns: [
    '__tests__/cssMocks.js',
    '__tests__/jest.setup.js',
    '__tests__/TestContext.tsx',
  ],
  testEnvironment: 'jsdom',
  transform: {
    '^.+\\.(js|jsx|mjs|cjs|ts|tsx)$': '<rootDir>/node_modules/babel-jest',
    '^.+\\.scss$': 'jest-scss-transform',
    '^.+\\.css$': '<rootDir>/__tests__/cssMocks.js',
  },
  transformIgnorePatterns: [
    '[/\\\\]node_modules[/\\\\].+\\.(js|jsx|mjs|cjs|ts|tsx)$',
    '^.+\\.module\\.(css|sass|scss)$',
  ],
  moduleNameMapper: {
    '^.+\\.module\\.(css|sass|scss)$': 'identity-obj-proxy',
  },
  watchPlugins: [
    'jest-watch-typeahead/filename',
    'jest-watch-typeahead/testname',
  ],
  resetMocks: true,
  testResultsProcessor: 'jest-sonar-reporter',
  cacheDirectory: '.jest/cache',
  reporters: ['default', 'jest-junit'],
};
