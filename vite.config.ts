import { defineConfig } from 'vite';
import reactRefresh from '@vitejs/plugin-react-refresh';

// https://vitejs.dev/config/
export default defineConfig({
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: [
          `@import "./src/scss/_variables"; @import "./src/scss/_mixins";`,
        ],
      },
    },
  },
  plugins: [reactRefresh()],
});
